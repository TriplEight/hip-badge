# Hacking in Parallel Badge
Hacking In Parallel (HiP) was a conference that happened in Berlin in December, 2022. All attendees received an electronic badge with many RGB LED Lights, USB, Battery Charging Control, NFC Radio, TVOC (Total Volatile Organic Compounds) air quality sensor, and a number of other way cool features.

# 37c3 [Workshop](https://events.ccc.de/congress/2023/hub/en/event/hacking-the-hip-badge/)

## Homework / step 1
This preparation step might take anywhere from 30 to 90 min so please prepare this beforehand or be prepared to use a USB stick.

`docker pull espressif/idf:v4.4.6`

And join our matrix channel: https://matrix.to/#/!gcVoVqNIOhOgaNUscO:matrix.org


![Alt text](documents/image.png)

## Come join your team

Assembly: HARDWARE HACKING AREA

Come to this workshop to learn about its embedded programming, and play with the free badges.

The participants will be organized into teams.
Novice and experienced hardware hackers are welcome to participate in a group/team of your choice.

The hardware is provided for free, one badge per team.

The badge devices are free to take home with you after the workshop showcase concludes.


## Docs
[Badge general description](documents/README.md)

[Firmware](firmware/README.md)

[Graphics](graphics/README.md)

[Hardware](hardware/README.md)

[Mechanic](mechanic/README.md)

[Software](software/README.md)
